<?php
/**
 * Initialize the custom theme options.
 */
add_action( 'init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
  
  /* OptionTree is not loaded yet, or this is not an admin request */
  if ( ! function_exists( 'ot_settings_id' ) || ! is_admin() )
    return false;
    
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array( 
    'contextual_help' => array( 
      'sidebar'       => ''
    ),
    'sections'        => array( 
      array(
        'id'          => 'front_page',
        'title'       => 'Front Page'
      ),
      array(
        'id'          => 'misc',
        'title'       => 'Misc'
      )
    ),
    'settings'        => array( 
      array(
        'id'          => 'featured_post',
        'label'       => 'Featured Post',
        'desc'        => '',
        'std'         => '',
        'type'        => 'post-select',
        'section'     => 'front_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'sticky_post',
        'label'       => 'Sticky Post',
        'desc'        => '',
        'std'         => '',
        'type'        => 'post-select',
        'section'     => 'front_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'featured_post_default_background',
        'label'       => 'Featured Post Default Background',
        'desc'        => 'Featured post background image will use its featured image by default, if its not available, use this image by default.',
        'std'         => '',
        'type'        => 'background',
        'section'     => 'front_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'single_post_default_background',
        'label'       => 'Single Post Default Background',
        'desc'        => 'Single post background image will use its featured image by default, if its not available, use this image by default.',
        'std'         => '',
        'type'        => 'background',
        'section'     => 'misc',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'archive_background',
        'label'       => 'Archive Background',
        'desc'        => 'Background image for archive (posts by author, category or tag) and search result header title.',
        'std'         => '',
        'type'        => 'background',
        'section'     => 'misc',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      )
    )
  );
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
  /* Lets OptionTree know the UI Builder is being overridden */
  global $ot_has_custom_theme_options;
  $ot_has_custom_theme_options = true;
  
}